# HousePool
Prototipo di tool per il bookmarking di abitazioni. 

## Stack Backend
* Lumen 5.4
* MySql
## Stack Frontend
* Vue.js
* axios

## Installazione
* Branch **Master** per produzione (build ottimizzato)
* Branch **Develop** per sviluppo

Copiare il file `.env.example` in `.env` andando a modificare gli opportuni parametri.
Eseguire `composer install`  
Per lo sviluppo eseguire anche `yarn install` e infine `php artisan migrate` per la creazione del DB.
 
## Sviluppo
`yarn run watch` per avviare e mantenere in background il task di compilazione degli assets di frontend.  
`yarn run build-dev` per compilazione una tantum non ottimizzata  
`yarn run build-prod` per compilazione una tantum ottimizzata  
`php vendor/bin/phunit` per l'esecuzione della suite di test backend  


let webpack = require('webpack');
let path = require('path');
let ExtractTextPlugin = require('extract-text-webpack-plugin');

let IS_DEV = process.env.NODE_ENV !== 'production';

const extract_sass = new ExtractTextPlugin({
    filename: '../css/app.css',
    allChunks: true
});
const common_chunks = new webpack.optimize.CommonsChunkPlugin({
    names: ['vendor']
});

module.exports = {
    entry: {
        app: './public/src/js/main.js',
        vendor: ['vue', 'axios']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    extractCSS: true,
                    loaders: {
                        'scss': 'vue-style-loader!css-loader!sass-loader',
                        'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
                    }
                }
            },
            {
                test: /\.(css|scss|sass)$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [{
                        loader: 'css-loader',
                        options: {
                            url: false,
                            sourceMap: IS_DEV,
                            minimize: !IS_DEV
                        }
                    }, {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: IS_DEV
                        }
                    }]
                })
            }
        ]
    },
    output: {
        path: path.resolve(__dirname, 'public/dist/js'),
        filename: '[name].js',
        publicPath: './public'
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    plugins: [
        common_chunks,
        extract_sass
    ],
    devtool: 'inline-source-map'
};

if (process.env.NODE_ENV === 'production') {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            sourcemap: true,
            compress: {
                warnings: false
            }
        })
    );

    module.exports.plugins.push(
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        })
    );
}
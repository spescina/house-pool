<?php

namespace App\Transformers;

use App\House;
use League\Fractal\TransformerAbstract;

class HouseTransformer extends TransformerAbstract
{
    public function transform(House $house)
    {
        return [
            'id'          => (int)$house->id,
            'label'       => $house->label,
            'address'     => $house->address,
            'location'    => [
                'lat' => (double)$house->location_lat,
                'lng' => (double)$house->location_lng,
            ],
            'price'       => (int)$house->price,
            'description' => $house->description,
            'link'        => $house->link,
            'type'        => $house->type,
            'note'        => $house->note,
            'rating'      => $house->rating,
        ];
    }
}
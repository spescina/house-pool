<?php

namespace App\Http\Controllers;

use App\House;
use App\Transformers\HouseTransformer;
use Dingo\Api\Http\Request;
use Dingo\Api\Routing\Helpers;

class APIController extends Controller
{
    use Helpers;

    public function index()
    {
        $houses = $this->auth->user()
            ->houses()->orderBy('id', 'desc')->get();

        return $this->response->collection($houses, new HouseTransformer);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'label'  => 'required|max:250',
            'lat'    => 'numeric',
            'lng'    => 'numeric',
            'rating' => 'numeric|min:1|max:4',
        ]);

        $data = $request->only([
            'label', 'lat', 'lng', 'price', 'description', 'address', 'link', 'type', 'rating', 'note'
        ]);

        $this->auth->user()->houses()->create([
            'label'        => $data['label'],
            'location_lat' => $data['lat'],
            'location_lng' => $data['lng'],
            'price'        => $data['price'],
            'description'  => $data['description'],
            'address'      => $data['address'],
            'link'         => $data['link'],
            'type'         => $data['type'],
            'rating'       => $data['rating'],
            'note'         => $data['note'],
        ]);

        return $this->response->created();
    }

    public function delete(Request $request, $id)
    {
        House::findOrFail($id)->delete();

        return $this->response->noContent();
    }

    public function show(Request $request, $id)
    {
        return $this->response->item(House::findOrFail($id), new HouseTransformer);
    }
}

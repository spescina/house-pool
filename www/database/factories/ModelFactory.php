<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name'           => $faker->name,
        'email'          => $faker->email,
        'password'       => app('hash')->make('johndoe'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\House::class, function (Faker\Generator $faker) {
    return [
        'user_id'      => function () {
            return factory(App\User::class)->create()->id;
        },
        'label'        => $faker->words(5),
        'address'      => $faker->address,
        'location_lat' => $faker->latitude,
        'location_lng' => $faker->longitude,
        'link'         => $faker->url,
        'description'  => $faker->paragraphs(3, true),
        'price'        => $faker->numberBetween(100000, 1000000),
        'note'         => $faker->paragraphs(1, true),
        'rating'       => $faker->numberBetween(1, 5),
        'type'         => $faker->randomElement(['house', 'apartment', 'villa', 'townhouse']),
    ];
});
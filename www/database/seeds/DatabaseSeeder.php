<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $john = factory(\App\User::class)->create([
            'email'    => 'johndoe@example.com',
            'password' => app('hash')->make('john'),
        ]);

        $jane = factory(\App\User::class)->create([
            'email'    => 'janedoe@example.com',
            'password' => app('hash')->make('jane'),
        ]);

        factory(App\House::class)->create([
            'user_id'      => $john->id,
            'label'        => 'House One',
            'address'      => 'Block 1',
            'location_lat' => 10.0,
            'location_lng' => 10.0,
            'link'         => 'https://www.google.it/#q=House+One',
            'price'        => 100000,
            'type'         => 'house',
            'rating'       => 1,
        ]);

        factory(App\House::class)->create([
            'user_id'      => $john->id,
            'label'        => 'House Two',
            'address'      => 'Block 2',
            'location_lat' => 20.0,
            'location_lng' => 20.0,
            'link'         => 'https://www.google.it/#q=House+Two',
            'price'        => 200000,
            'type'         => 'apartment',
            'rating'       => 2,
        ]);

        factory(App\House::class)->create([
            'user_id'      => $jane->id,
            'label'        => 'House Three',
            'address'      => 'Block 3',
            'location_lat' => 30.0,
            'location_lng' => 30.0,
            'link'         => 'https://www.google.it/#q=House+Three',
            'price'        => 300000,
            'type'         => 'villa',
            'rating'       => 3,
        ]);

        factory(App\House::class)->create([
            'user_id'      => $john->id,
            'label'        => 'House Four',
            'address'      => 'Block 4',
            'location_lat' => 40.0,
            'location_lng' => 40.0,
            'link'         => 'https://www.google.it/#q=House+Four',
            'price'        => 400000,
            'type'         => 'townhouse',
            'rating'       => 4,
        ]);
    }
}

<?php

use Tymon\JWTAuth\Facades\JWTAuth;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * @var Faker\Generator
     */
    protected $faker;

    public function setUp()
    {
        $this->faker = Faker\Factory::create();

        parent::setUp();

        $this->artisan('migrate');
        $this->artisan('db:seed');
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }

    protected function authenticate(App\User $user = null)
    {
        if (!$user) {
            $user = App\User::where('email', 'johndoe@example.com')->first();
        }

        JWTAuth::setToken(JWTAuth::fromUser($user));

        return JWTAuth::fromUser($user);
    }

    protected function user()
    {
        return app('Dingo\Api\Auth\Auth')->user();
    }
}

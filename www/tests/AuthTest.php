<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    public function testLoginWithWrongValidation()
    {
        $this->json('post', 'api/auth/login', [
            'email'    => $this->faker->email,
            'password' => 'asdasd'
        ])->seeJson([
            'message' => 'invalid_credentials'
        ])->assertResponseStatus(401);
    }

    public function testCorrectLogin()
    {
        $email = $this->faker->email;
        $password = str_random(8);

        factory(\App\User::class)->create([
            'email'    => $email,
            'password' => app('hash')->make($password)
        ]);

        $this->json('post', 'api/auth/login', [
            'email'    => $email,
            'password' => $password
        ])
            ->assertResponseOk();
    }

    public function testCreateUser()
    {
        $email = $this->faker->email;

        $this->json('post', 'api/auth/user', [
            'email'                 => $email,
            'password'              => 'asdasd',
            'password_confirmation' => 'asdasd',
        ])->assertResponseStatus(201);

        $this->seeInDatabase('users', ['email' => $email]);
    }

    public function testCreateInvalidUser()
    {
        $email = $this->faker->email;

        $this->json('post', 'api/auth/user', [
            'email'                 => $email,
            'password'              => 'asdasd',
            'password_confirmation' => 'asd',
        ])->assertResponseStatus(500);

        $this->notSeeInDatabase('users', ['email' => $email]);
    }

}

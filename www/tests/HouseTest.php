<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class HouseTest extends TestCase
{
    use DatabaseTransactions;

    public function testCreateUserHouse()
    {
        $token = $this->authenticate();

        $this->json('post', "api/houses?token={$token}", [
            'label'       => 'My House',
            'type'        => 'house',
            'address'     => 'My address',
            'lat'         => 45.0,
            'lng'         => 45.0,
            'link'        => 'https://www.google.it/#q=My+House',
            'description' => 'lorem ipsum',
            'price'       => 100000,
            'note'        => 'lorem ipsum',
            'rating'      => 2,
        ])->assertResponseStatus(201);

        $this->assertEquals(4, $this->user()->houses->count());
    }

    public function testCreateInvalidUserHouse()
    {
        $token = $this->authenticate();

        $this->json('post', "api/houses?token={$token}", [
            'label' => '',
            'lat'   => 12.0,
            'lng'   => 12.0,
        ])->assertResponseStatus(500);
    }

    public function testFetchLoggedUserHouses()
    {
        $token = $this->authenticate();

        $this->json('get', "api/houses?token={$token}")
            ->seeJsonStructure([
                'data' => [
                    '*' => [
                        'label',
                        'address',
                        'type',
                        'location',
                        'link',
                        'description',
                        'price',
                        'note',
                        'rating',
                    ]
                ]
            ])
            ->seeJsonContains([
                'label'    => 'House One',
                'address'  => 'Block 1',
                'location' => [
                    'lat' => 10.0,
                    'lng' => 10.0,
                ],
                'link'     => 'https://www.google.it/#q=House+One',
                'price'    => 100000,
                'type'     => 'house',
            ])->seeJsonContains([
                'label'    => 'House Two',
                'address'  => 'Block 2',
                'location' => [
                    'lat' => 20.0,
                    'lng' => 20.0,
                ],
                'link'     => 'https://www.google.it/#q=House+Two',
                'price'    => 200000,
                'type'     => 'apartment',
            ])->dontSeeJson([
                'label' => 'House Three',
            ])
            ->assertResponseStatus(200);
    }
}

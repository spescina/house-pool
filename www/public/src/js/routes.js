import Vue from "vue";
import VueRouter from "vue-router";
import {DashboardPage, NewHousePage, LoginPage} from "./pages/index";
import store from "./store";
import AuthService from "./services/auth";
import ApiService from "./services/api";

Vue.use(VueRouter);

const router = new VueRouter({
    base: '/app/',
    routes: [
        {path: '/', component: DashboardPage, name: 'dashboard'},
        {path: '/new-house', component: NewHousePage, name: 'new-house'},
        {path: '/auth/login', component: LoginPage, name: 'login'},
        {path: '*', redirect: '/'}
    ]
});

const firstRun = () => {
    let user = AuthService.loadFromLocalStorage();

    if (user) {
        AuthService.login(user.email, user.token);
        ApiService.refreshToken().then((response) => {
            AuthService.login(user.email, response.data.data.token);
        }).catch((error) => {
            router.push({name: 'login'});
        });
    }
};

router.beforeEach((to, from, next) => {
    // if not logged in go to login page, not to requested
    if (!store.state.user.token) {
        if (to.name !== 'login') {
            return next({name: 'login'});
        }
    }
    // if already logged go to dashboard, not to login
    else if (to.name === 'login') {
        return next({name: 'dashboard'});
    }

    // go to the requested route
    return next();
});

firstRun();

export default router;
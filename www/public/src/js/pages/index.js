import DashboardPage from "./DashboardPage.vue";
import LoginPage from "./LoginPage.vue";
import NewHousePage from "./NewHousePage.vue";

export {
    DashboardPage,
    LoginPage,
    NewHousePage,
};
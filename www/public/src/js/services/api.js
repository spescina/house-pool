import axios from "axios";
import store from "../store";

let api = () => {
    return axios.create({
        headers: {'Authorization': 'Bearer ' + store.state.user.token}
    });
};

export default {
    attemptLogin(email, password) {
        return axios.post('/api/auth/login', {
            email: email,
            password: password
        });
    },
    refreshToken() {
        return api().patch('/api/auth/refresh');
    },
    storeUser(user) {
        return axios.post('/api/auth/user', {
            email: user.email,
            password: user.password,
            password_confirmation: user.password_confirmation,
        });
    },
    fetchHouses() {
        return api().get('/api/houses').then((response) => {
            store.commit('loadedHouses', response.data.data);
        });
    },
    storeHouse(house) {
        return api().post('/api/houses', {
            label: house.label,
            address: house.address,
            price: house.price,
            description: house.description,
            lat: house.location[0],
            lng: house.location[1],
            link: house.link,
            type: house.type,
            rating: house.rating,
            note: house.note,
        });
    },
    deleteHouse(id) {
        return api().delete('/api/houses/' + id);
    },
};


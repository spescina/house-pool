import store from "../store";

export default {
    loadFromLocalStorage() {
        let email = localStorage.getItem('userEmail');
        let token = localStorage.getItem('userToken');

        return token ? {email, token} : false;
    },

    login(email, token) {
        store.commit('loginUser', {
            email,
            token,
        });

        localStorage.setItem('userEmail', email);
        localStorage.setItem('userToken', token);
    },

    logout() {
        store.commit('logoutUser');

        localStorage.removeItem('userEmail');
        localStorage.removeItem('userToken');
    },
}

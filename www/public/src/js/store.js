import Vuex from "vuex";
import Vue from "vue";

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        user: {
            email: false,
            token: false,
        },
        houses: []
    },
    mutations: {
        loginUser (state, user) {
            state.user.email = user.email;
            state.user.token = user.token;
        },
        logoutUser (state) {
            state.user.email = false;
            state.user.token = false;
        },
        loadedHouses (state, houses) {
            state.houses = houses;
        }
    }
});

export default store;